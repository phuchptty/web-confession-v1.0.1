<?php
/* >_ Developed by Vy Nghia */
require 'lib/class/confession.class.php';

/* WEBSITE DOMAIN */
define('WEBURL', 'http://domain.com');

/* MYSQL DATABASE */
$db = new Database;
$db->dbhost('localhost');
$db->dbuser('username');
$db->dbpass('password');
$db->dbname('db_name');

$db->connect();

/* CALL APP SDK */
include ('app.fb.php');